(4) the moral state of fallen man is described in Scripture Gen_6:5; 1Ki_8:46; Psa_14:1-3; Psa_39:5; Jer_17:9; Mat_18:11; Mar_7:20; Mar_7:23; Rom_1:21; Rom_2:1-29; Rom_3:9-19; Rom_7:24; Rom_8:7; Joh_3:6; 1Co_2:14; 2Co_3:14; 2Co_4:4; Gal_5:19-21; Eph_2:1-3; Eph_2:11; Eph_2:12; Eph_4:18-22; Col_1:21; Heb_3:13; Jas_4:14; 1Co_15:22.

Outline:

1. what is sin?
2. consequences of sin : suffering, death, eternal damnation
3. sin is the biggest problem : why the world will not change
4. GOD's solution for sin
5. Jesus and nothing else
6. Conclusion (I put before you life and death, choose life)


Verses:

Hosea 13:4 (God wants you to put Him first because he is the only one that can save you)
